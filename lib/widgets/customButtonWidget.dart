import 'package:flutter/material.dart';
import 'package:flutter_first_app/core/const.dart';

class CustemButtonWidget extends StatelessWidget {
  final Widget child;
  final double size;
  final double borderWidth;
  final String image;
  final VoidCallback onTap;
  final bool isActive;

  CustemButtonWidget(
      {
        this.child,
        @required this.size,
        this.borderWidth=2,
        this.image,
        @required this.onTap,
        this.isActive=false,

      });

  @override
  Widget build(BuildContext context) {
    var boxDecoration =BoxDecoration(
      borderRadius: BorderRadius.all(
        Radius.circular(200),
      ),
      border:Border.all(
        width: borderWidth,
        color: Color(0xff2D142C),
      ),
      boxShadow: [
        BoxShadow(
          color: Color(0xff696e7c),
          blurRadius: 5,
          offset: Offset(4,4),
          spreadRadius: 2,
        ),
        BoxShadow(
          color: Color(0xff8f939d),
          blurRadius: 4,
          offset: Offset(-4,-4),
          spreadRadius: 2,
        ),
      ],

    );

    if(image != null)
      {
        boxDecoration=boxDecoration.copyWith(
            image: DecorationImage(
                image: ExactAssetImage(image),
                fit: BoxFit.cover,
            ),
        );
      }

    if(isActive)
      {
        boxDecoration=boxDecoration.copyWith(
          gradient: RadialGradient(
            colors:[
              Color(0xffEE4540),
              Color(0xffEE4540),
              Color(0xffC72C41),
              Color(0xff801236),


            ],

          ),
        );
      }
    else
      {
        boxDecoration=boxDecoration.copyWith(
          gradient: RadialGradient(
            colors:[
              Color(0xffEE4540),
              Color(0xffEE4540),
              Color(0xffC72C41),
              Color(0xff801236),
              Color(0xff510A32),
              Color(0xff2D142C),

            ],

          ),
        );

      }




    return Container(
      width: size,
      height: size,
      decoration: boxDecoration,
      child: FlatButton(
        padding: EdgeInsets.all(0),
        onPressed: onTap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(200)),
        ),
        child: child?? Container(),
      ),
    );


  }
}
