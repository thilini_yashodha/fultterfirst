import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_first_app/core/const.dart';
import 'package:flutter_first_app/widgets/customButtonWidget.dart';
import 'package:flutter_first_app/model/toneModel.dart';

class Tones extends StatefulWidget {
  @override
  _TonesState createState() => _TonesState();
}

class _TonesState extends State<Tones> {
  int _playID;
  List<ToneModel> _list;

  @override
  void initState() {
    _playID=-1;
   _list=ToneModel.list;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
         // color: Color(0xff801336)
        color:  new Color(0xffB00020),
        gradient: LinearGradient(

            colors: [new Color(0xff2D142C),new Color(0xff610A32)],
            begin: Alignment.centerRight,
            //end: Alignment.centerLeft,
            end: new Alignment(-1.0, -1.0)

        ),
      ),
      child: SafeArea(
        child: Scaffold(
        // backgroundColor: AppColors.mainColor,
          body: Container(
            decoration: BoxDecoration(
              color:  new Color(0xffB00020),
              gradient: LinearGradient(

                  colors: [new Color(0xff2D142C),new Color(0xff610A32)],
                  begin: Alignment.centerRight,
                  //end: Alignment.centerLeft,
                  end: new Alignment(-1.0, -1.0)

              ),

            ),
            padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 30),
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CustemButtonWidget(

                            child: CircleAvatar(

                              backgroundColor: Color(0xff000000),
                              radius: 60,
                              child: Image.asset(
                                'assets/Image/logo.png',
                                height: 100,
                                width: 125,
                                fit: BoxFit.fitWidth,
                              ),

                            ),

                          ),


                        ],
                      ),
                    ),
                    Expanded(
                      child: Card(
                        color: Colors.black38.withOpacity(0.1),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)
                        ),
                        child: ListView.builder(
                          padding: EdgeInsets.only(left: 20.0,right: 20.0,top: 20.0,bottom: 50),
                         // padding: EdgeInsets.all(20),
                          physics:BouncingScrollPhysics(),
                          itemCount: _list.length,
                          itemBuilder: (context,index){
                            return AnimatedContainer(
                              duration: Duration(milliseconds: 400),
                              decoration: BoxDecoration(
                                  color:_list[index].id==_playID
                                      ?const Color(0xff510A32).withOpacity(0.4)
                                      :Colors.transparent,
                              borderRadius: _list[index].id==_playID
                                  ?BorderRadius.all(Radius.circular(10))
                                  :BorderRadius.all(Radius.circular(0)),
                              ),
                              padding: EdgeInsets.all(16),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(_list[index].title,style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                      ),
                                      ),
                                    ],
                                  ),
                                  CustemButtonWidget(
                                    child: Icon(
                                      _list[index].id==_playID ?Icons.pause :Icons.play_arrow,
                                      color: _list[index].id==_playID ? Colors.white : Colors.white,
                                      size:25 ,
                                    ),
                                    size: 45,

                                    isActive: _list[index].id==_playID,

                                  onTap: () {
                                      setState(() {
                                        _playID= _list[index].id;
                                      });

                                    },



                                  ),
                                ],
                              ),
                            );
                          },
                        ),



                      ),
                    )
                  ],
                ),

                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    height: 60,
                   // padding: EdgeInsets.only(left: 25,right: 25),
                    width: MediaQuery.of(context).size.width-20,
                    decoration:BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          AppColors.mainColor.withAlpha(0),
                          AppColors.mainColor,
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,

                      ),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)
                      ),
                    ),

                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
