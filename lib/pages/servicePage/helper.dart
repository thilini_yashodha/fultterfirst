import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:expansion_card/expansion_card.dart';

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {

  List<Person> persons = [
    Person(title: 'HOW TO USE THE LAUNCH CODE™ TEMPO TRAINER', data: "Congrats on taking a step towards a better swing!!Using this app is super simple. We have outlined some steps below to get you started.",image:'assets/Image/swing2.jpg'),
    Person(title: 'FINDING YOUR TEMPO', data: "1.	To get started, the first thing we want to do is find the tempo closest to our current swing tempo.  To do this, we are going to start with the 30/10 tempo and work our way down through the other tempos.  Press the button to get the notes going, grab a club, and start trying to match the notes to your own swing. \n\n2.	The first note corresponds to the start of your golf swing.  The second, the top of your backswing.  The third, impact. \n\n3.	You will know quickly if a tempo feels off or close to the speed of your current swing.  Regardless, continue to proceed through the rest of the tempo speeds.  Doing this will help you get a feel for the tempo that best suits your current swing and allow you to experience what it may be like to swing faster.",image:'assets/Image/swing5.jpg'),
    Person(title: 'ONCE YOU KNOW YOUR TEMPO', data: "Now that you have picked the tempo you are going to train with, let’s get started.  \n\n A quick note.  Some of you may have issues at first.  The tempo may feel off or you may find it hard to hit the notes.  My word of advice is this, STAY WITH IT!  Any type of golf training takes time and tempo training is no different.  Give yourself the space to be off at first, but know that you will master your tempo. \n\nThe biggest issue that many of you will have with tempo training, has to do with hitting the impact note.  This is common for a lot of golfers, as they either have too much of a body move from the top or hold their lag for too long.  If you believe you may be having this problem, head on over to our website and visit the “Tempo Training” page.  On the tempo training page, we will detail some tips and tricks to help you with your quest to master tempo training.",image:'assets/Image/swing1.png'),
    Person(title: 'HOW MUCH SHOULD I TRAIN?', data: "This answer is completely dependent on you.  I would recommend at least once a day, for at least 5-10 minutes, but work it in when you can!  There is no amount that is too much necessarily, but burnout is real.  So, take it slow and make it a part of your routine, not the whole routine. \n\n What is even more important than how much you are training; is how you are training.  There are two ways to implement training.  The first, is to just swing along to the tempo, trying to match the three positions to the notes.  If this is all you have time for, do it as much as you can.  However, if you have the time, the second way is the best option. The second option is to hit balls utilizing the tempo trainer. \n\nMy recommended routine when training on the range is as follows: \n Make three swings without a ball and then one with a ball.  Repeat this cycle over and over, switching clubs as often as every other shot.  We want to recreate the actual game as much as possible. Switching clubs every other shot will help us with that. \n\n Just remember, tempo training should be a part of your training session…not your whole training session.  I recommend starting each training session working on your tempo.  It is a great way to setup the rest of your training session for success.",image:'assets/Image/swing4.jpg'),
    Person(title: 'TIPS', data: "Here are some some tips to keep in mind. \n\n 1.	If you have them, utilize earbuds while practicing tempo in public\n2.	Utilize the tempo trainer prior to playing.\n3.	When training, try to start mimicking the notes and tempo either in your head or verbally.  This will help should you need something out on the course to tap into.",image:'assets/Image/swing3.jpg'),
    Person(title: 'ONE FINAL TIP', data: "HAVE FUN!  GOLF IS HARD.  STAY POSITIVE AND YOUR GAME WILL REAP THE REWARDS.",image:'assets/Image/swing6.jpg')
  ];

  Widget personDetailCard(Person) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ExpansionCard(
        borderRadius: 10,
        background: Image.asset(
          Person.image,
          fit: BoxFit.fill,
          height: 250,
        ),

        title: Container(
          padding: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.black87,
                Colors.black87,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  Person.title,
                  style: TextStyle(
                      fontFamily: 'BalooBhai', fontSize: 20, color: Colors.white),
                      textAlign: TextAlign.center,
                ),
              ],
            ),
            padding: const EdgeInsets.only(left: 3.0,right: 3.0),
            alignment: Alignment.center,
          ),
        ),
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius:  BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)),
              gradient: LinearGradient(
                  colors: [
                    Colors.black54,
                    Colors.black87,
                  ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              ),
            ),

            child: Text(Person.data,
                style: TextStyle(
                    fontFamily: 'BalooBhai',
                    fontSize: 17,
                    color: Colors.white)),
                    padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 10.0),

          )
        ],
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        // color: Color(0xff801336)
        color:  new Color(0xffB00020),
        gradient: LinearGradient(

            colors: [new Color(0xff2D142C),new Color(0xff610A32)],
            begin: Alignment.centerRight,
            //end: Alignment.centerLeft,
            end: new Alignment(-1.0, -1.0)

        ),
      ),
      child: SafeArea(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
              color:  new Color(0xffB00020),
              gradient: LinearGradient(

                  colors: [new Color(0xff2D142C),new Color(0xff610A32)],
                  begin: Alignment.centerRight,
                  //end: Alignment.centerLeft,
                  end: new Alignment(-1.0, -1.0)

              ),

            ),
            padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 30,top: 30),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                  children: persons.map((p) {
                    return personDetailCard(p);
                  }).toList()
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class Person {
String title;
String data;
String image;

Person({this.title,this.data,this.image});
}
