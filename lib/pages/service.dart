import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_first_app/pages/servicePage/tone.dart';
import 'package:flutter_first_app/pages/servicePage/helper.dart';
import 'package:flutter_first_app/core/const.dart';

class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {

  int _selectPage=0;
  final _pageOption=[
    Tones(),
    Help(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageOption[_selectPage],
      bottomNavigationBar: SizedBox(
        height: 70,
        child: CurvedNavigationBar(
          backgroundColor: Color(0xff2D142C),
          color: Colors.black87.withOpacity(0.85),
          items: <Widget>[
            Icon(Icons.audiotrack,size: 30,color: Colors.white),
            Icon(Icons.help_outline,size: 30,color: Colors.white,),
          ],
          onTap: (int index)
          {
            setState(() {
              _selectPage=index;
            });
          },
        ),
      ),


    );

  }

}




