import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 8),() => Navigator.pushNamed(context, "/service"));

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Container(
        decoration: BoxDecoration(
          //color: Color(0xff801336)
          color:  new Color(0xffB00020),
          gradient: LinearGradient(

              colors: [new Color(0xff2D142C),new Color(0xff610A32)],
              begin: Alignment.centerRight,
              //end: Alignment.centerLeft,
              end: new Alignment(-1.0, -1.0)

          ),
        ),
        child: SafeArea (
          child: Stack(
            fit:  StackFit.expand,
            children: <Widget>[

              Container(
                decoration: BoxDecoration(
                  color:  new Color(0xffB00020),
                  gradient: LinearGradient(

                      colors: [new Color(0xff2D142C),new Color(0xff610A32)],
                      begin: Alignment.centerRight,
                      //end: Alignment.centerLeft,
                      end: new Alignment(-1.0, -1.0)

                  ),

                ),

              ),

              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  CircleAvatar(
                    backgroundColor: Color(0xff000000),
                    radius: 115,
                    child: Image.asset('assets/Image/logo.png',
                      height: 150,
                      width: 175,
                      fit: BoxFit.fitWidth,
                    ),

                  )

                ],

              ),

            ],
          ),
        ),
      ),
    );
  }
}



