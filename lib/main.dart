import 'package:flutter/material.dart';
import 'package:flutter_first_app/pages/splashScreen.dart';
import 'package:flutter_first_app/pages/service.dart';

var routes=<String,WidgetBuilder>
{
   "/service":(BuildContext context)=> Services()
};

void main() => runApp(MaterialApp(
  title: 'Tempo Train',
  theme: ThemeData(
    primaryColor:  new Color(0xff2D142C),
    accentColor: new Color(0xff20142c),
    scaffoldBackgroundColor: const Color(0xff2D142C),

  ),
  debugShowCheckedModeBanner: false,
  home: SplashScreen(),
  routes: routes,

));


