import 'package:flutter/cupertino.dart';

class AppColors {
  static const mainColor = Color(0XFF000000);
  static const styleColor = Color(0XFFB00020);
  static const activeColor = Color(0XFFd0ddf3);
  static const lightBlue = Color(0XFF92aeff);
  static const darkBlue = Color(0XFF510A32);
  static const lightBlueShadow = Color(0XAA92aeff);
}