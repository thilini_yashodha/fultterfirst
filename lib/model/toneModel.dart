class ToneModel {
  final int id;
  final String title;
  final String album;
  final double duration;

  ToneModel({this.id, this.title, this.album, this.duration});

  static List<ToneModel> list = [
    ToneModel(
      id: 1,
      title: "  15/5",
      duration: 60,
    ),
    ToneModel(
      id: 2,
      title: "  18/6",
      duration: 60,
    ),
    ToneModel(
      id: 3,
      title: "  21/7",
      duration: 60,
    ),
    ToneModel(
      id: 4,
      title: "  24/8",
      duration: 60,
    ),
    ToneModel(
      id: 5,
      title: "  27/9",
      duration: 60,
    ),
    ToneModel(
      id: 6,
      title: "  30/10",
      duration: 60,
    ),

  ];
}